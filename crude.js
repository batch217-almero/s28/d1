// CRUD Operations
// Create, Read, Update, Delete

// CREATE (Insert Documents) ---------------------------------
// Insert 1 Item
db.users.insertOne({
	firstName: "Jane",
	lastName: "Doe",
	age: 21,
	contact: {
		phone: "87654321",
		email: "janedoe@mail.com"
	},
	courses: ["CSS", "Javascript", "Python"],
	department: "None"
});

// Insert 2 Items
db.users.insertMany([
	{
		firstName: "Stephen",
		lastName: "Hawking",
		age: 76,
		contact: {
			phone: "87654321",
			email: "stephenhawking@gmail.com"
		},
		courses: ["Python", "React", "PHP"],
		department: "none"
	},

	{
		firstName: "Neil",
		lastName: "Armstrong",
		age: 82,
		contact: {
			phone: "87654321",
			email: "neilarmstrong@gmail.com"
		},
		courses: ["React", "Laravel", "Sass"],
		department: "none"
	}

]);


// READ (Finding Documents) ---------------------------------
db.users.find(); //will display all documents in collection

db.users.find({firstName: "Stephen"}); //will find a specific document in collection

db.users.find({lastName: "Armstrong", age: 82}); //find document with multiple parameter


// UPDATE (Updating 1 Document) ---------------------------------
// Create dummy document
db.users.insertOne({
	firstName: "Test",
	lastName: "Test",
	age: 0,
	contact: {
		phone: "000000",
		email: "test@gmail.com"
	},
	courses : [],
	department: "none"
});

// Update dummy document
db.users.updateOne(
	{firstName: "Test"},
	{
		$set: {
			firstName: "Bill",
			lastName: "Gates",
			age: 65,
			contact: {
				phone: "12345678",
				email: "bill@mail.com"
			},
			courses: ["PHP", "Laravel", "HTML"],
			department: "Operations",
			status: "active"
		}
	}
);

// UPDATE (Updating Multiple Document) ---------------------------------
db.users.updateMany(
	{department: "none"},
	{
		$set: {
			department: "HR"
		}
	}
);


// MINI ACTIVITY 
// Create dummy document
db.users.insertOne({
	firstName: "Dhonel",
	lastName: "Almero",
	age: 23,
	contact: {
		phone: "09285701117",
		email: "dhonel15s@gmail.com"
	},
	courses : ["HTML", "CSS", "Javascript"],
	department: "Web Development"
});


// DELETE (Delete 1 Document) ---------------------------------
// Create dummy document
db.users.insertOne({
	firstName: "Test",
	lastName: "Test",
	age: 0,
	contact: {
		phone: "000000",
		email: "test@gmail.com"
	},
	courses : [],
	department: "none"
});


// Delete dummy document
db.users.deleteOne({firstName: "Test"});

// DELETE (Delete Multiple Documents) ---------------------------------
// Create dummy documents
db.users.insertOne({
	firstName: "Test",
	lastName: "Test",
	age: 0,
	contact: {
		phone: "000000",
		email: "test@gmail.com"
	},
	courses : [],
	department: "Security"
});

db.users.insertOne({
	firstName: "Test2",
	lastName: "Test2",
	age: 0,
	contact: {
		phone: "000000",
		email: "test@gmail.com"
	},
	courses : [],
	department: "Security"
});

db.users.deleteMany({
	department: "Security"
});


// REPLACE - Pag may namali sa property name
db.users.replaceOne(
	{firstName: "Bill" },
	{ firstName: "Mark"}
);
// NOTE: If one property lang ilalagay sa replace,mawawala yung ibang entry bukod sa firstname

// QUERY (or READ) - How to query and NESTED DOCUMENT (document inside a document)
db.users.find({
	contact: {
		phone: "87654321",
		email: "stephenhawking@gmail.com"
	}
})

// Sample 2 //Use double quote "" to access using dot notation
db.users.find(
	{"contact.email" : "janedoe@mail.com"}
);

// QUERY (or READ) from an Array with exact elements
db.user.find(
	{courses: ["CSS", "Javascript", "Python"]}
);

// QUERY (or READ) from an Array with exact elements (REGARDLESS OF ORDER)
db.user.find(
	{courses: {$all: ["React", "Python", "PHP"]}}
);


// INSERT TO AN ARRAY
db.users.insertOne({
	namearr: [
		{
			namea: "Juan"
		},
		{
			nameb: "Tamad"
		}
	]
});


// FIND IN ARRAY
db.users.find({
	namearr: 
		{
			namea: "Juan"
		}
});